import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();

const items: Array<any> = [
  {
    name: "T-shirt",
    description: 'Great fit, super comfy',
    image: 'https://i.imgur.com/keQtXT3.png',
    price: 25,
    sku: '123'
  },
  {
    name: 'Sweater',
    description: "Awesome for cold days",
    image: 'https://i.imgur.com/fVpadKg.png',
    price: 50,
    sku: '456',
  },
  {
    name: 'Button Down',
    description: 'A little fancier',
    image: 'https://i.imgur.com/JxGBvvj.png',
    price: 30,
    sku: '759'
  }
];

const main = async () => {
  for (let item of items) {
    await prisma.product.create({ data: item})
  }
}

main()
  .catch(err => {
  console.log(err);
  process.exit(1);
  })
  .finally(() => {
    prisma.$disconnect();
  })

/*
 * Prisma Schema
 * npx prisma db seed --preview-feature
 * and
 * npx prisma studio
 */
