# NX Stack Typescript

## Installation

```bash
npm i
```

## Launch
API Rest server
```bash
nx run api:serve
```
Back Office
```bash
nx run backoffice:serve:development
```

## Add dependencies

For show all dependencies installed and available

```bash
nx list
```

For install dependencies

```bash
npm i @nrwl/<PACKAGE>
```

and

```
nx generate @nrwl/<PACKAGE>
```

> ### Notes for commands from nx documentation
> Picture from nx documentation for generate a plugin:
> ![](https://nx.dev/documentation/shared/angular-tutorial/generator-syntax.svg)
>
> Picture from nx documentation for launch command:
> ![](https://nx.dev/documentation/shared/angular-tutorial/run-target-syntax.svg)

## Database

By default database is manage by prisma on Prisma/schema.prisma

```prisma
// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}
```

for create sqlite file for sample:

```bash
npx prisma db push --preview-feature
```

For Migrate data

```bash
npx prisma migrate dev
```

## Seed

Add typescript file like `seed.ts` and use command
seed.ts

```typescript
import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();

const items: Array<any> = [
  {
    name: "T-shirt",
    description: 'Great fit, super comfy',
    image: 'https://i.imgur.com/keQtXT3.png',
    price: 25,
    sku: '123'
  },
  {
    name: 'Sweater',
    description: "Awesome for cold days",
    image: 'https://i.imgur.com/fVpadKg.png',
    price: 50,
    sku: '456',
  },
  {
    name: 'Button Down',
    description: 'A little fancier',
    image: 'https://i.imgur.com/JxGBvvj.png',
    price: 30,
    sku: '759'
  }
];

const main = async () => {
  for (let item of items) {
    await prisma.product.create({data: item})
  }
}

main()
  .catch(err => {
    console.log(err);
    process.exit(1);
  })
  .finally(() => {
    prisma.$disconnect();
  })

/*
 * Prisma Schema
 * npx prisma db seed --preview-feature
 * and
 * npx prisma studio
 */
```

```bash
npx prisma db seed --preview feature
```

For create an entity in Nest for example:

```bash
nx generate @nrwl/nest:library products --controller --service
```

In products.service.ts for import PrismaClient and our model data.

```typescript
import {PrismaClient, Product} from "@prisma/client";
```

For generate a component in angular, we have need three things:

1. New project call `ui` for example
2. New Angular component into this project
3. New Angular service into this project

### Project

```bash
nx generate @nrwl/angular:lib ui
```

and once it's done. You can generate a component and service

```bash
nx g component products --project=ui --export
```

```bash
nx g service products --project=ui
```

### Testing

Push seed and execute command for Project Name

```bash
npx nx test <PROJECT_NAME_OR_LIBS>
```

| Project Name | Description (Framework) | Folder Name         | Library Folders Name   |
|-------------:|:------------------------|:--------------------|:-----------------------|
|          api | API REST  (Nest)        | apps/**api**        | libs/**products** <br> |
|   backoffice | Back Office (Angular)   | apps/**backoffice** | lib/**ui**             |

