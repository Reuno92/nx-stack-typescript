import { Component } from '@angular/core';

@Component({
  selector: 'nx-stack-typescript-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'backoffice';
}
