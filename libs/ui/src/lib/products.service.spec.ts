import {TestBed} from '@angular/core/testing';

import {ProductsService} from './products.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {HttpClientModule} from "@angular/common/http";

describe('ProductsService', () => {
  let service: ProductsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
    });
    service = TestBed.inject(ProductsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return all products', () => {
    const DATA = [
      {
        name: "T-shirt",
        description: 'Great fit, super comfy',
        image: 'https://i.imgur.com/keQtXT3.png',
        price: 25,
        sku: '123'
      },
      {
        name: 'Sweater',
        description: "Awesome for cold days",
        image: 'https://i.imgur.com/fVpadKg.png',
        price: 50,
        sku: '456',
      },
      {
        name: 'Button Down',
        description: 'A little fancier',
        image: 'https://i.imgur.com/JxGBvvj.png',
        price: 30,
        sku: '759'
      }
    ];

    const expected = service.product$.getValue();
    expect(expected).toBe(DATA);
  });
});
