import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';
import {ProductsComponent} from './products/products.component';
import {ProductsService} from './products.service';
import {HttpClientModule} from '@angular/common/http';
import localeFr from '@angular/common/locales/fr';
import {CreateProductComponent} from './create-product/create-product.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

registerLocaleData(localeFr);

@NgModule({
  imports: [CommonModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  declarations: [ProductsComponent, CreateProductComponent],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'fr-FR',
    },
    ProductsService,
  ],
  exports: [ProductsComponent, CreateProductComponent],
})
export class UiModule {
}
