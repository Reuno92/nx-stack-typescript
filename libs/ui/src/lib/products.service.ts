import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {Product} from "@prisma/client";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private API_URL: string = `http://localhost:3333/api`;

  public product$: BehaviorSubject<Array<Product>>;

  constructor(
    private http: HttpClient
  ) {
    this.product$ = new BehaviorSubject<Array<Product>>([]);
  }

  public getProducts(): void {
    this.http.get<Array<Product>>(`${this.API_URL}/products`).subscribe(
      data => this.product$.next(data),
      err => console.log(err)
    );
  }

  public createProduct(body: Omit<Product, 'id'>): void {
    this.http.post<never>(`${this.API_URL}/products`, body).subscribe(
      () => this.getProducts(),
      (err) => console.log(err),
      () => console.log('finished')
    );
  }
}
