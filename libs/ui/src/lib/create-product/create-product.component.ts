import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ProductsService} from "../products.service";

@Component({
  selector: 'nx-stack-typescript-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
})
export class CreateProductComponent implements OnInit {

  public formProduct: FormGroup;

  constructor(
    private fb: FormBuilder,
    private productService: ProductsService,
  ) {
    this.formProduct = this.fb.nonNullable.group({
      name: new FormControl<string>('Spider Man T-shIrt', [Validators.required]),
      description: new FormControl<string>('Amazing Spidey T-shirt for kids, women and men', [Validators.required, Validators.maxLength(250)]),
      image: new FormControl<string>('https://cdn.shopify.com/s/files/1/0549/0085/0876/products/T-Shirt-Iron-Spiderman_800x.jpg', [Validators.required]),
      price: new FormControl<number>(0, [Validators.required]),
      sku: new FormControl<string>('263E72ZSHJDBQ72TR8A3FE', [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.formProduct.valueChanges.subscribe( data => console.dir(data, { depth: null, colors: true, compact: true}));
  }

  public getCard(): boolean {
    return this.formProduct.get('name')?.value !== '' &&
           this.formProduct.get('description')?.value !== '' &&
           this.formProduct.get('image')?.value !== '' &&
           this.formProduct.get('price')?.value > 0
  }

  public sendProduct(): void {
    const DATA = this.formProduct.getRawValue()
    console.log('SEND', DATA);

    this.productService.createProduct(DATA);
    this.formProduct.reset();
  }

}
