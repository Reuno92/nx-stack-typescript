import { Component, OnInit } from '@angular/core';
import { ProductsService } from "../products.service";
import {BehaviorSubject, Observable} from "rxjs";
import { Product } from '@prisma/client';

@Component({
  selector: 'nx-stack-typescript-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {

  public products$: BehaviorSubject<Array<Product>> = this.productsService.product$;

  constructor(
    public productsService: ProductsService
  ) {}

  ngOnInit() {
    this.productsService.getProducts();
  }
}
