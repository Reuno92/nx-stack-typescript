import { Injectable } from '@nestjs/common';
import { PrismaClient, Product } from "@prisma/client";

const prisma = new PrismaClient();

@Injectable()
export class ProductsService {
  public getProducts(): Promise<Array<Product>> {
    return prisma.product.findMany()
      .catch( (err: Error) => {
        throw Error(err?.message)
      });
  }

  public getOneProduct(id: string): Promise<Product> {
    return prisma.product.findUniqueOrThrow({ where: { id: id }})
      .catch( (err: Error) => {
        throw Error(err?.message)
      });
  }

  public postProduct(data: Omit<Product, 'id'>): Promise<Product> {
    return prisma.product.create({
      data: data,
    }).catch( (err: Error) => {
        throw Error(err?.message)
      });
  }

  public modifyProduct(id: string, data: Omit<Product, 'id'>): Promise<Product> {
    return prisma.product.update({
      where: { id: id },
      data: data
    }).catch( (err: Error) => {
        throw Error(err?.message)
      });
  }

  public deleteProduct(id: string): Promise<Array<Product>> {
    return prisma.product.delete({
      where: { id: id }
    }).then( () => this.getProducts())
      .catch( (err: Error) => {
        throw Error(err?.message)
      });
  }
}
