import { Test } from '@nestjs/testing';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import {Product} from "@prisma/client";

describe('ProductsController', () => {
  let controller: ProductsController;

  let productId: string;

  const BASE_URL = 'http://localhost:3333/api/products';
  const ALL_DATA: Promise<Array<Product>> = fetch(BASE_URL).then( x => x.json());
  const DATA: Omit<Product, 'id'> = {
    "name": "Amazing T-shirt",
    "description": "Awesome T-shirt with spider-man",
    "image": "https://cdn.shopify.com/s/files/1/0549/0085/0876/products/T-Shirt-Iron-Spiderman_800x.jpg",
    "price": 25,
    "sku": "234568"
  };

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [ProductsService],
      controllers: [ProductsController],
    }).compile();

    controller = module.get(ProductsController);
  });

  it('should be defined', () => {
    expect(controller).toBeTruthy();
  });

  it('should return all data', async () => {
    const RECEIVED = await controller.getProducts();
    expect(JSON.stringify(RECEIVED)).toStrictEqual(JSON.stringify(await ALL_DATA));
  });

  it('should return a product when you create it', async () => {
    expect(await controller.postProducts(DATA).then(x => {
      productId = x.id;
      return x;
    })).toStrictEqual({
      ...await DATA,
      id: productId
    });
  });

  it('should return a product by its id', async() => {
    const EXPECTED = {
      id: productId,
      ...DATA
    }
    expect(await controller.getOneProduct(productId).then(x => x)).toStrictEqual(EXPECTED);
  });

  it('should return a modify product', async () => {
    const NEW_DATA: Omit<Product, 'id'> = {
      ...DATA,
      description: 'Amazing spidey t-shirt',
      sku: 'ZYVDZU12345678HSDFSQU'
    };

    const EXPECTED: Product = {
      id: productId,
      ...NEW_DATA
    };

    expect(await controller.putProducts(productId, NEW_DATA)).toStrictEqual(EXPECTED);
  });

  it('should return all data after delete one product by its id', async () => {

    const RECEIVED = await controller.removeProduct(productId);

    console.dir(RECEIVED === await ALL_DATA);

    expect(JSON.stringify(RECEIVED)).toStrictEqual(JSON.stringify(await ALL_DATA));
  });
});
