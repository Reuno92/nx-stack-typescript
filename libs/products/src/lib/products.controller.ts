import { Controller, Get, Post, Body, Put, Param, Delete, HttpCode } from '@nestjs/common';
import { ProductsService } from './products.service';
import {Product} from "@prisma/client";

@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}

  @Get()
  public async getProducts(): Promise<Array<Product>> {
    return this.productsService.getProducts();
  }

  @Get()
  public async getOneProduct(@Param('id') id: string): Promise<Product> {
    return this.productsService.getOneProduct(id);
  }

  @Post()
  @HttpCode(201)
  public async postProducts(@Body() data: Omit<Product, 'id'>): Promise<Product> {
    console.log('DATA RECEIVED', data);
    return this.productsService.postProduct(data);
  }

  @Put(':id')
  public async putProducts(@Param('id') id: string, @Body() data: Omit<Product, 'id'>): Promise<Product> {
    return this.productsService.modifyProduct(id, data);
  }

  @Delete(':id')
  public async removeProduct(@Param('id') id: string): Promise<Array<Product>> {
    return this.productsService.deleteProduct(id);
  }
}
